# TIPS AND TRICKS

A collection of useful commands I discovered over time.

Contents:

[TOC]

# ASCII/Unicode

Find all non-ASCII characters in a file:

```bash
grep --color='auto' -P -n '[^\x00-\x7F]' SRCFILE
```

Convert all non-ASCII characters to ASCII equivalents:

```bash
iconv --from-code UTF-8 --to-code US-ASCII//translit -c SRCFILE > DSTFILE
```

# Mercurial

Find what a specific person committed in the last week.

```bash
hg log --user "Someone" -d "-6"
```

# JavaScript

Trap alert() calls before they happen:

```JavaScript
window.alert = function() { debugger; }; // Trick to trap alert() calls.
```

# Git

Fix wrong username/email in submitted changelists (from [GitHub](https://help.github.com/articles/changing-author-info/)):

```bash
#!/bin/sh

git filter-branch -f --env-filter '

OLD_EMAIL="furst.last@dumbain.dom"
CORRECT_NAME="FirstName LastName"
CORRECT_EMAIL="first.last@domain.dom"

if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi

' --tag-name-filter cat -- --branches --tags
```

Note that I've added a `-f` flag to allow fixing multiple cases before submitting.

Once it is fixed (use `git log` to confirm), they recommend pushing using:

```bash
git push --force --tags origin 'refs/heads/*'
```

# Docker

Remove images that haven't completed:

```bash
docker rmi -f $(docker images -q --filter "dangling=true")
```

Runs a shell in a Docker image:

```bash
docker run -it genvidtech/nativesdk /bin/sh
```

Runs a shell in the first dangling image:

```bash
docker run -it $(docker images -q --filter "dangling=true" | head -1) /bin/sh
```

Deletes all of the unused images:

```bash
docker system prune --all
```

Some useful routines:

```bash
# Removes all dangling images.
function dcd {
    imgs=$(docker images -q --filter "dangling=true")
    if [ ! -z $imgs ]; then
        docker rmi -f ${imgs}
    else
        echo "ERROR: No dangling image..."
    fi
}
```

```bash
# Opens a shell to the specified image.
function dss {
    if [ ! -z $1 ]; then
        docker run -it ${1} /bin/sh
    else
        echo "ERROR: Missing container ID."
    fi
}
```

```bash
# Opens a shell to the latest dangling image.
function dsd {
    img=$(docker images -q --filter "dangling=true" | head -1)
    if [ ! -z $img ]; then
        dss ${img}
    else
        echo "ERROR: No dangling image..."
    fi
}
```